/*
 * Copyright (C) 2021  Tiago Simões Tomé
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ESP32_WiFi_LightSwitcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * ESP32_WiFi_LightSwitcher Web Server for LED-Lamps, WS2812B (NeoPixel) and other things
 *
 * A simple web server that lets you switch LEDs, Lamps and other things via the web.
 * This sketch will print the IP address of your WiFi Shield (once connected)
 * to the Serial monitor. From there, you can open that address in a web browser
 * to turn on and off the LED on pin 5.
 *
 * This example is written for a network using WPA encryption. For
 * WEP or WPA, change the Wifi.begin() call accordingly.
 *
 *
*/

#include "analogWrite.h"
#include <WiFi.h>
#include <Adafruit_NeoPixel.h>
#include <OneWire.h>
#include <DallasTemperature.h>


const char* ssid     = "Lab's Network";
const char* password = "";

// set port to 80
WiFiServer server(80);

// Variable to store HTTP request
String header;


// NeoPixel LED (WS2812B) configuration
#define LED_PIN    17    // set data pin for WS2812B LEDs
#define LED_COUNT  6     // set Number of LEDs
// set max. Brightness to use with setBrightness() function,
// can be used as a throttle if LEDs are to Bright
#define BRIGHTNESS 20    // use Values from 1 to 255
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// LED Pins
const unsigned int led5 = 5;    // pin5 = led5 :P

// LED State variables
String led5State = "off";
String ledWSState = "off";

// OneWire Data Pin for DS18B20 Temperature Sensors
#define ONE_WIRE_BUS 16
// Setup a oneWire instance to communicate with any OneWire devices
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// Temperature Variables for DS18B20
float temp0;    // 1st sensor
float temp1;    // 2nd sensor

// set Time values for timer()
unsigned long previousMillis0 = 0;    // previousMillis for WebServerTimeoutTimer
const long timeout = 2000;            // WebServerTimeout
unsigned long previousMillis1 = 0;    // previousMillis for tempTimer
const long interval1 = 800;           // tempTimerInterval


void setup() {
    Serial.begin(115200);
    pinMode(led5, OUTPUT);            // set the LED pin mode to OUTPUT
    digitalWrite(led5, LOW);    // set the LED(-pin) to default initial State (LOW)
    //analogWriteResolution(led5, 12);  // set analogWrite(PWM) resolution of Pin5 to 12-bit

    sensors.begin();
    strip.begin();
    strip.show();
    strip.setBrightness(BRIGHTNESS);

    delay(10);


    Serial.println("\n####################################");
    Serial.print(" ESP32_WiFi_LightSwitcher ");
    Serial.print("v0.1.1");
    Serial.println("   #\n####################################");
    // Connect to a WiFi Network
    Serial.print(" Connecting to ");
    Serial.print(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.print("\n WiFi connected to ");
    Serial.print(ssid);
    Serial.print(".\n IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println("####################################");
    server.begin();

}

int value = 0;

void loop() {
  WiFiClient client = server.available();   // listen for incoming clients

  unsigned long currentMillis = millis();
  unsigned int timer0 = timer(currentMillis, previousMillis0);    // WebServerTimeoutTimer
  unsigned int timer1 = timer(currentMillis, previousMillis1);    // TempTimer

  // Loop to read 1-Wire Thermometer(s) (DS18B20)
  if (timer1 >= interval1) {
    previousMillis1 = currentMillis;
    // get values from DS18B20 Thermometer
    getTemperature();
  }

  if (client) {                             // if you get a client,
    previousMillis0 = currentMillis;        // set timer0() back
    Serial.println(" @@\nNew Client.");     // print a message out the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected() && timer0 <= timeout) {   // while the client's connected AND in Timeout time
      currentMillis = millis();             // update currentMillis for timer() works right on while Loop
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();


            // Check to see if the client request was "GET /H" or "GET /L":
            if (header.indexOf("GET /D5/H") >= 0) {
              led5State = "on";
              digitalWrite(led5, HIGH);          // GET /H (HIGH) turns the LED on
              Serial.println("\nLED 5 ON!");
            }
            if (header.indexOf("GET /D5/L") >= 0) {
              led5State = "off";
              digitalWrite(led5, LOW);                // GET /L (LOW) turns the LED off
              Serial.println("\nLED 5 OFF!");
            }

            if (header.indexOf("GET /D5/0") >= 0) {
              analogWrite(led5, 0);
              Serial.println("\nLED 5 Dim 0");
            }
            if (header.indexOf("GET /D5/01") >= 0) {
              analogWrite(led5, 10);
              Serial.println("\nLED 5 Dim 10");
            }
            if (header.indexOf("GET /D5/1") >= 0) {
              analogWrite(led5, 100);
              Serial.println("\nLED 5 Dim 100");
            }
            if (header.indexOf("GET /D5/5") >= 0) {
              analogWrite(led5, 500);
              Serial.println("\nLED 5 Dim 500");
            }
            if (header.indexOf("GET /D5/10") >= 0) {
              analogWrite(led5, 1000);
              Serial.println("\nLED 5 Dim 1000");
            }
            if (header.indexOf("GET /D5/20") >= 0) {
              analogWrite(led5, 2000);
              Serial.println("\nLED 5 Dim 2000");
            }
            if (header.indexOf("GET /D5/30") >= 0) {
              analogWrite(led5, 3000);
              Serial.println("\nLED 5 Dim 3000");
            }
            if (header.indexOf("GET /D5/40") >= 0) {
              analogWrite(led5, 4095);
              Serial.println("\nLED 5 Dim 4095");
            }

            if (header.indexOf("GET /WS/OFF") >= 0) {
              colorWipe(strip.Color(0, 0, 0), 50); // OFF
              Serial.println("\nLED_STRIP OFF");
            }
            if (header.indexOf("GET /WS/RAIN") >= 0) {
              rainbowFade2White(10, 7, 0);
              Serial.println("\nLED_STRIP Rainbow");
            }
            if (header.indexOf("GET /WS/WHITE") >= 0) {
              colorWipe(strip.Color(255, 255, 255), 50); // True white
              Serial.println("\nLED_STRIP WHITE");
            }
            if (header.indexOf("GET /WS/RED") >= 0) {
              colorWipe(strip.Color(255, 0, 0), 50); // Red
              Serial.println("\nLED_STRIP RED");
            }
            if (header.indexOf("GET /WS/GREEN") >= 0) {
              colorWipe(strip.Color(0, 255, 0), 50); // Green
              Serial.println("\nLED_STRIP GREEN");
            }
            if (header.indexOf("GET /WS/BLUE") >= 0) {
              colorWipe(strip.Color(0, 0, 255), 50); // Blue
              Serial.println("\nLED_STRIP BLUE");
            }
            if (header.indexOf("GET /WS/PURPLE") >= 0) {
              colorWipe(strip.Color(100, 0, 255), 50); // Purple
              Serial.println("\nLED_STRIP PURPLE");
            }
            if (header.indexOf("GET /WS/PINK") >= 0) {
              colorWipe(strip.Color(255, 0, 255), 50); // Pink
              Serial.println("\nLED_STRIP PINK");
            }

            // HTML Web Page
            client.println("<!DOCTYPE html><html>");
            client.print("<head><title>ESP WiFi LightSwitcher @ ");
            client.print(ssid);
            client.print("</title><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.print("<link rel=\"icon\" href=\"data:,\">");
            // CSS
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println("body {background-color: #1F1F1F; color: #F1F1F1;}");      // Use Darkmode
            client.println("h1 {background-color: #2A2A2A; color: #F1F1F1}");         // h1 design
            client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");  // ON-Button (Green)
            client.println("text-decoration: none; font-size: 20px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}");                  // OFF/Neutral-Button
            client.println(".buttonR {background-color: #FF0000;}");                  // Red-Button
            client.println(".buttonG {background-color: #00FF00;}");                  // Green-Button
            client.println(".buttonB {background-color: #0000FF;}");                  // Blue-Button
            client.println(".buttonPU {background-color: #6400FF;}");                 // Purple-Button
            client.println(".buttonPI {background-color: #FF00FF;}</style></head>");  // Pink-Button

            // Web Page Heading
            client.println("<body><h1>LightSwitcher</h1>");

            client.println("<p>LED5 is " + led5State + "</p>");
            if (led5State=="off") {
              client.println("<p><a href=\"/D5/H\"><button class=\"button\">ON</button></a></p>");
            }
            else {
              client.println("<p><a href=\"/D5/L\"><button class=\"button button2\">OFF</button></a></p>");
            }

            client.print("It's ");
            client.print(temp1);
            client.print(" &deg;C Inside.<br>");
            client.print("It's ");
            client.print(temp0);
            client.print(" &deg;C Outside.<br>");

            client.println("<h2>WS2812B</h2>");
            client.println("<p><a href=\"/WS/OFF\"><button class=\"button button2\">OFF</button></a></p>");
            client.println("<p><a href=\"/WS/RAIN\"><button class=\"button button2\">RAINBOW</button></a></p>");
            client.println("<p><a href=\"/WS/WHITE\"><button class=\"button button2\">WHITE</button></a></p>");
            client.println("<p><a href=\"/WS/RED\"><button class=\"button buttonR\">RED</button></a></p>");
            client.println("<p><a href=\"/WS/GREEN\"><button class=\"button buttonG\">GREEN</button></a></p>");
            client.println("<p><a href=\"/WS/BLUE\"><button class=\"button buttonB\">BLUE</button></a></p>");
            client.println("<p><a href=\"/WS/PURPLE\"><button class=\"button buttonPU\">PURPLE</button></a></p>");
            client.println("<p><a href=\"/WS/PINK\"><button class=\"button buttonPI\">PINK</button></a></p>");

            client.print("<br>Dim pin 5 to <a href=\"/D5/0\">0</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/01\">10</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/1\">100</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/5\">500</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/10\">1000</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/20\">2000</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/30\">3000</a> / 4095.<br>");
            client.print("Dim pin 5 to <a href=\"/D5/40\">4095</a> / 4095.<br>");

            client.println("</body></html>");

            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          }
          else {    // if you got a newline, then clear currentLine:
            currentLine = "";
          }
        }
        else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear header variable
    header = "";
    // close the connection:
    client.stop();
    Serial.println("Client Disconnected.");
    Serial.println("####################");
  }
}


void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}


void rainbowFade2White(int wait, int rainbowLoops, int whiteLoops) {
  int fadeVal=0, fadeMax=100;

  // Hue of first pixel runs 'rainbowLoops' complete loops through the color
  // wheel. Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to rainbowLoops*65536, using steps of 256 so we
  // advance around the wheel at a decent clip.
  for(uint32_t firstPixelHue = 0; firstPixelHue < rainbowLoops*65536;
    firstPixelHue += 256) {

    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }

    strip.show();
    delay(wait);

    if(firstPixelHue < 65536) {                              // First loop,
      if(fadeVal < fadeMax) fadeVal++;                       // fade in
    } else if(firstPixelHue >= ((rainbowLoops-1) * 65536)) { // Last loop,
      if(fadeVal > 0) fadeVal--;                             // fade out
    } else {
      fadeVal = fadeMax; // Interim loop, make sure fade is at max
    }
  }

  for(int k=0; k<whiteLoops; k++) {
    for(int j=0; j<256; j++) { // Ramp up 0 to 255
      // Fill entire strip with white at gamma-corrected brightness level 'j':
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
    delay(1000); // Pause 1 second
    for(int j=255; j>=0; j--) { // Ramp down 255 to 0
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
  }

  delay(100); // Pause 1/2 second
}


unsigned long timer(unsigned long currentMillis, unsigned long previous) {
  unsigned long t = currentMillis - previous;
  return t;
}

void getTemperature() {
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus

  //Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperature readings
  //Serial.println("DONE");
  temp0 = sensors.getTempCByIndex(0); // Why "byIndex"?
  temp1 = sensors.getTempCByIndex(1);
  // You can have more than one DS18B20 on the same bus.
  // 0 refers to the first IC on the wire
  //Serial.print("Temp0: "); Serial.println(temp0);
  //Serial.print("Temp1: "); Serial.println(temp1);
}
