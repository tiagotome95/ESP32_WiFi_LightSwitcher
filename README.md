# ESP32 WiFi LightSwitcher

Control your own ESP32 SmartLamps and Nerdgets over an WebGUI.

This Project is an Extention of my wired SmartHome Project and contain parts of it and of other of my Projects too, all the projects are licensed with GNU GPL3 and the Licenses choosen by the Authors of the used Libraries (A Thank for this Authors :D Please Support the Authors of Software Code by Donate or buy official Hardware from Arduino and Adafruit for Example and Support the makers of your IDE and Libraries, you can Support Developer too by testing software and give feedback of issues, or just txt me if you code :D )


This Software don't use safe Transfer of packets over WiFi, it just uses your Wireless LAN encryption, so everyone in your LAN can acess to the device, I consider this as unsafe/unsecure and just use it for Developement, but SSL can be integrated in future!
Just trusted Domais and encrypted Transfer makes trusted and secure servers/devices, so don't run this software on production! ;P

## License

Copyright (C) 2021  Tiago Simões Tomé

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

ESP32_WiFi_LightSwitcher is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Description

ESP32_WiFi_LightSwitcher is a Project to develope Web based:
  * SmartLamps,
  * Switcher,
  * Dimmer,
  * SmartRemoteController,
  * noAI-Robots like:
   * Toys,
   * RC-Cars,
   * RC-Caterpillars,
   * Drones, etc.
  * Ambilight,
  * SmartHome and SmartGarden

so we go work on an WebGUI that help us to:

* Switch Lamps on/off with Relays (Please contact the electrician you trust!)
* Switch Lamps on/off with MOSFETs and use PWM-function to work with dimmable LEDs
* Control RGB-LED-Strips, WS2812B and other NeoPixel compatible LEDs
* Output roomTemperature, humidity and other sensor data and use it to interact with other things :P
* Control Servos, Stepper Motor, and other (with Gear calculation for non-Pneumatic/Hydraulic Catapilars)
* Keep using the old RS-485 and CAN bus-systems for SmartHomeUtilities where wires comes better than WiFi
* Read and Write Data to be restored later, like: configFiles, Statistics or just the HTML-, CSS-, JS-code and the ICONs for the WebServer
* Make various(2) Tasks, that run one on its own Core, to take use of both Cores from ESP32's microprocessor and get more performance at the WebInterfaceSide while Hardware tasks getting done :D

## Dependencies || Get Started

You should install Arduino IDE >= 1.8.3
<https://www.arduino.cc/en/software>

**Add ESP32 as additional Board:**

Go to `File > Preferences` under `Additional Boards Manager URLs:` and cp the sources index.json URL from manufacturer:
```
https://dl.espressif.com/dl/package_esp32_index.json
```

Go to `Tools > Board > Boards Manager`, search for 'esp32' and install:
`esp32 by Espressif Systems`

Go to `Tools > Board > ESP32 Arduino > ESP32 Dev Module`
Go to `Tools > Port > /dev/ttyUSB*` to choose the port of your ESP32 (* can be 0)


**Libraries:**
* `WiFi.h` for WiFi and Webserver
* Adafruit NeoPixel as `Adafruit_NeoPixel.h` for WS2812B LED-Strips
* ESP32 ESP32S2 AnalogWrite by David Lloyd as `analogWrite.h` for PWM
* OneWire & Dallas Temperature as `OneWire.h` and `DallasTemperature.h` for OneWire

**Tools:**
* `SPIFFS` (Serial Peripheral Interface Flash File System)
